var app = require('express')();
var server = require('http').createServer(app);
var exec = require('child_process').exec;
var path = require('path'),
    childProcess = require('child_process');

var languages = {
  'afrikaans':'af',
  'albanian':'sq',
  'arabic':'ar',
  'armenian':'hy',
  'azerbaijani':'az',
  'basque':'eu',
  'belarusian':'be',
  'bengali':'bn',
  'bosnian':'bs',
  'bulgarian':'bg',
  'catalan':'ca',
  'cebuano':'ceb',
  'chichewa':'ny',
  'chinese':'zh-CN',
  'croatian':'hr',
  'czech':'cs',
  'danish':'da',
  'dutch':'nl',
  'english':'en',
  'esperanto':'eo',
  'estonian':'et',
  'filipino':'tl',
  'finnish':'fi',
  'french':'fr',
  'galician':'gl',
  'georgian':'ka',
  'german':'de',
  'greek':'el',
  'gujarati':'gu',
  'haitian creole':'ht',
  'hausa':'ha',
  'hebrew':'iw',
  'hindi':'hi',
  'hmong':'hmn',
  'hungarian':'hu',
  'icelandic':'is',
  'igbo':'ig',
  'indonesian':'id',
  'irish':'ga',
  'italian':'it',
  'japanese':'ja',
  'javanese':'jw',
  'kannada':'kn',
  'kazakh':'kk',
  'khmer':'km',
  'korean':'ko',
  'lao':'lo',
  'latin':'la',
  'latvian':'lv',
  'lithuanian':'lt',
  'macedonian':'mk',
  'malagasy':'mg',
  'malay':'ms',
  'malayalam':'ml',
  'maltese':'mt',
  'maori':'mi',
  'marathi':'mr',
  'mongolian':'mn',
  'myanmar (burmese)':'my',
  'nepali':'ne',
  'norwegian':'no',
  'persian':'fa',
  'polish':'pl',
  'portuguese':'pt',
  'punjabi':'pa',
  'romanian':'ro',
  'russian':'ru',
  'serbian':'sr',
  'sesotho':'st',
  'sinhala':'si',
  'slovak':'sk',
  'slovenian':'sl',
  'somali':'so',
  'spanish':'es',
  'sundanese':'su',
  'swahili':'sw',
  'swedish':'sv',
  'tajik':'tg',
  'tamil':'ta',
  'telugu':'te',
  'thai':'th',
  'turkish':'tr',
  'ukrainian':'uk',
  'urdu':'ur',
  'uzbek':'uz',
  'vietnamese':'vi',
  'welsh':'cy',
  'yiddish':'yi',
  'yoruba':'yo',
  'zulu':'zu'
}

app.get('/:to/:word', function(req,res,next) {
    
    var from = languages[req.param('from')];
    var to  = languages[req.params.to];
    if(from == undefined){
        from = 'en';
        var word = req.params.word.replace(/[^\w\s]/gi, '');
    }else{
        var word = req.params.word.replace(/！/g, '!');
        word = word.replace(/。/g, '.');
        word = word.replace(/[_+-.,!@#$%^&*();\/|<>"']/g, " ");
    }
    var pathInput = "phantomjs/bin/phantomjs google.js "+from+" "+to+" "+encodeURI(word);
    var absoluteCommand = path.join(__dirname,pathInput);
    console.log(__dirname);
    console.log(from,to,word);

    childProcess.exec(absoluteCommand,
        function(err, out, code) {
            if (err instanceof Error)
                throw err;
                // console.log(out);
        out=out.split('\n')[0]
        exec(['python application.py "'+out+'"'], function(err, out, code){
                res.send(out);
                if(err)
                console.log('err\n',err)
                if(code)
                console.log('stderr\n',code)

        });
        }
    );
    // exec(['phantomjs google.js '+lang+' '+req.params.word], function(err, out, code){
    // });
});

server.listen(process.env.PORT || 8080,function(){});
