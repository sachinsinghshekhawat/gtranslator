#! /usr/bin/python
# -*- coding: utf-8 -*-

import requests
import json
import sys, io, os, types
import codecs
from collections import OrderedDict
from datetime import timedelta
from functools import update_wrapper

if sys.version_info >= (3, 0):
  basestring = (str,bytes)

def get_data(http_url, word, lang):
  wordss=[]
  # length = len(words_list.split())

  FIRST_FLAG = True
  # headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36'}

  s = requests.Session()

  # lang = languages[lang]
  # a = s.get('https://translate.google.com/#en/'+lang+'/mouse')
  jsondata = u''
  # f.write('{')
  jsondata += '{'
  # try:
  # http_url = 'https://translate.google.com/translate_a/single?client=t&sl=en&tl='+lang+'&hl=en&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&otf=1&rom=1&ssel=0&tsel=0&kc=5&tk='+tk+'&q='+word
  # print(http_url)
  b = s.get(http_url)

  wordss.append(word)
  bb = b.text
  # print(bb.encode('utf-8'))
  if not FIRST_FLAG:
    jsondata+=','
    # f.write(',')
  else:
    FIRST_FLAG = False
  # f.write('"'+word+'":',)
  jsondata += '"'+word+'":'
  # f.write(bb.encode('utf-8'))
  jsondata += bb
  # except Exception as e:
    # print("Error in word",word+'. Fix it manually after completion.' )
    # print(e)
  # f.write('}')
  jsondata += '}'
  return jsondata

def clean(data):
  # with open(filename, 'r') as f:

  while data.find(',,')>0:
    data = data.replace(',,',',')

  while data.find('[,')>0:
    data = data.replace('[,','[')

  while data.find(',]')>0:
    data = data.replace(',]',']')

  while data.find('\\u003cb\\u003e')>0:
    data = data.replace('\\u003cb\\u003e','\'')

  while data.find('\\u003c/b\\u003e')>0:
    data = data.replace('\\u003c/b\\u003e','\'')

  return data

def parser(language_,data):
  # filename = lang+'.json'
  # print(data)
  a = json.JSONDecoder(object_pairs_hook=OrderedDict).decode(data)
  # a = json.loads(data)

  dictionary = OrderedDict()

  faults = {}
  output_dict = OrderedDict()
  output_dict['language'] = language_

  for i in a:
    try:
      try:
        output_dict['meaning'] = a[i][0][0][0]
        # print(str(a[i][0][0][0]).encode('utf-8'))
      except:
        pass
      try:
        # a[i][0][1][1]
        if a[i][1]=='en':
          faults[i] = True
          # print('f')
          # raise Exception('No meaning found')
        else:
          # print(a[i][1][0][1][0],a[i][0][0][0])
          if len(a[i][1])==1 and a[i][1][0][0]=="" and len(a[i][1][0][1])==1 and a[i][1][0][1][0]==a[i][0][0][0]:
            raise Exception('No meaning found')
        dictionary[i] = a[i]
      except:
        pass
        # print(a[i][0][0][1],)
        # del a[i]
        # break
    except:
      print('error')
      print(a[i][0][0])
      # break

  # print(len(dictionary))

  

  # # word = dictionary['act']

  for i in dictionary:
    dictionary_element = OrderedDict()

    translation = OrderedDict()
    definition = OrderedDict()
    synonym = OrderedDict()
    example = []
    temp_synonyms = {}

    word = dictionary[i]

    if i not in faults:
      try:
        for j in word[1]:
          translation[j[0]] = OrderedDict()
          for k in j[2]:
            translation[j[0]][k[0]] = k[1]
      except:
        pass

    pass #word[2]
    pass #word[3]

    INITIAL = 4

    if i in faults:
      INITIAL = 3


    try:
      word[INITIAL]+1
      #word[4] is useless. (cooling)
      INITIAL = INITIAL + 1 #Become 5

      if word[INITIAL][1][0]==word[INITIAL - 1]:
        INITIAL = INITIAL + 1 #Become 6
        pass
        #word[5] is useless. (parents)
      else:
        pass
        if word[INITIAL][2][0]==1:
          #word[6] is useless. (woman)
          INITIAL = INITIAL + 2
        else:
          raise Exception('Dunno what')

    except TypeError:
      #word[4] is useful (a)
      INITIAL = 4
    except IndexError:
      pass

    SYNONYM = True
    DEFINITION = True
    EXAMPLE = True

    try:
      if isinstance(word[INITIAL][0][2],basestring):
        try:
          i = word[INITIAL][0][2]
        except:
          pass
        # SYNONYM OR DEFINITION        
        pass
        if not isinstance(word[INITIAL][0][1][0][0],basestring):
          # HAS SYNONYM
          # print('Synonym',)

          for j in word[INITIAL]:
            synonym[j[0]] = []
            for k in j[1]:
              synonym[j[0]].append([k[1],k[0]])
              

          pass
          INITIAL+=1

        if isinstance(word[INITIAL][0][1][0],list) and isinstance(word[INITIAL][0][1][0][0],basestring):
          # HAS DEFINITION
          # print('Definition',)


          for j in word[INITIAL]:
            definition[j[0]] = []
            for l,k in enumerate(j[1]):
              definition_collection = OrderedDict()
              definition_collection['meaning'] = k[0]
              try:
                definition_collection['example'] = k[2]
              except:
                # NO EXAMPLE
                pass
              try:
                if k[1]!='':
                  definition_collection['synonym'] = k[1]
              except:
                # NO SYNONYM
                pass
              definition[j[0]].append(definition_collection)
              # temp_synonyms[k[1]] = l
          pass
          INITIAL+=1


      else:
        raise IndexError('NotSynOrDef')
    except IndexError:
      # print('!!',i)
      pass
      # NEITHER SYNONYM NOR DEFINITION

    try:
      if isinstance(word[INITIAL][0][0],list):
        for j in word[INITIAL][0]:
          example.append(j[0])
    except:
      pass

    try:
      dictionary_element['translation']=translation
    except:
      pass
      # print(i,'translation')
    try:
      dictionary_element['definition']=definition
    except:
      pass
      # print(i,'definition')
    try:
      dictionary_element['synonym']=synonym
    except:
      pass
      # print(i,'synonym')
    try:
      dictionary_element['example']=example
    except:
      pass
      # print(i,'example')

    output_dict[i] = dictionary_element

  # with codecs.open('temp.json', "wb", encoding='utf-8') as f:
    # json.dump(output_dict,f, ensure_ascii=False)

  jsondata = json.dumps(output_dict, ensure_ascii=False)
  return jsondata

# @app.route('/')
# @crossdomain(origin='*')
def main():
  # with open('temp.json', 'r') as f:
    # data = f.read() 
  # lang = request.args['language']
  # word = request.args['word']
  url = sys.argv[1]
  # word = sys.argv[2]
  # lang = sys.argv[3]
  lang = url[66:68]
  word = url[url.index('q=')+2:]
  # word = 
  jsondata = get_data(url,word,lang)
  jsondata = clean(jsondata)
  data = parser(lang,jsondata)
  print(data.encode('utf-8'))
  return data

if __name__ == '__main__':
  main();
  # port = int(os.environ.get("PORT", 5000))
  # app.run(debug=True, port=80)
